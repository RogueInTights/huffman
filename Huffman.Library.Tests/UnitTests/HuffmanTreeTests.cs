﻿using Huffman.Library.Utilities;
using NUnit.Framework;
using System;

namespace Huffman.Library.Tests.UnitTests
{
    [TestFixture]
    class HuffmanTreeTests
    {
        [Test]
        public void Constructor_ThrowsArgumentNullException()
        {
            HuffmanTree tree;

            var ex = Assert.Throws<ArgumentNullException>(
                () => tree = new HuffmanTree(null)
            );

            string messageExpected = "An array of 256 elements is required\r\nИмя параметра: frequencies";
            string messageActual = ex.Message;

            Assert.AreEqual(messageExpected, messageActual);
        }

        [Test]
        public void Constructor_ThrowsArgumentException_IfFrequenciesContainsLessOrMoreThen256Items()
        {
            HuffmanTree tree;

            var ex = Assert.Throws<ArgumentException>(
                () => tree = new HuffmanTree(new int[] { 0, 1, 2 })
            );

            string messageExpected = "An array of 256 elements is required\r\nИмя параметра: frequencies";
            string messageActual = ex.Message;

            Assert.AreEqual(messageExpected, messageActual);
        }

        [Test]
        public void Constructor_ThrowsArgumentException_IfAllFrequenciesValuesDoNotExceed0()
        {
            HuffmanTree tree;

            int[] frequencies = new int[256];

            var ex = Assert.Throws<ArgumentException>(
                () => tree = new HuffmanTree(frequencies)
            );

            string messageExpected = "Frequencies must contain at least one value greater than 0\r\nИмя параметра: frequencies";
            string messageActual = ex.Message;

            Assert.AreEqual(messageExpected, messageActual);
        }

        [Test]
        public void HuffmanTree_CreatesCorrectTableOfReplacements()
        {
            // Data to construct HuffmanTree
            int[] frequencies = new int[256];

            frequencies[0] = 1;
            frequencies[1] = 2;
            frequencies[2] = 4;
            frequencies[3] = 5;
            frequencies[4] = 6;

            // Get replacement map from HuffmanTree
            HuffmanTree tree = new HuffmanTree(frequencies);

            tree.CreateReplacementsTable();

            bool[][] replacementsActual = tree.Replacements;

            // Expected replacement map
            string[] replacementsExpected = new string[] { "111", "110", "10", "01", "00" };

            for (byte i = 0; i <= 4; i++)
            {
                byte symbol = i;

                if (replacementsActual[symbol] == null)
                {
                    Assert.Fail($"The replacement table doesn't contain the key \"{symbol}\"");

                    continue;
                }

                string expected = replacementsExpected[symbol];
                string actual = replacementsActual[symbol].ToBinaryString();

                Assert.AreEqual(expected, actual);
            }
        }
    }
}
