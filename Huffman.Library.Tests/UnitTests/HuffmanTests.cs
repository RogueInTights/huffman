﻿using Huffman.Library.Utilities;
using NUnit.Framework;
using System;

namespace Huffman.Library.Tests.UnitTests
{
    [TestFixture]
    class HuffmanTests
    {
        [Test]
        public void EncodeMethod_ThrowsArgumentNullException()
        {
            var ex = Assert.Throws<ArgumentNullException>(
                () => HuffmanCode.Encode(null)
            );

            string messageExpected = "Source must not be null\r\nИмя параметра: source";
            string messageActual = ex.Message;

            Assert.AreEqual(messageExpected, messageActual);
        }

        [Test]
        public void EncodeMethod_ThrowsArgumentException_IfSourceIsEmpty()
        {
            var ex = Assert.Throws<ArgumentException>(
                () => HuffmanCode.Encode(new byte[] { })
            );

            string messageExpected = "Source must not be empty\r\nИмя параметра: source";
            string messageActual = ex.Message;

            Assert.AreEqual(messageExpected, messageActual);
        }

        [Test]
        public void DecodeMethod_ThrowsArgumentNullException()
        {
            // Prepare HuffmanTree
            int[] frequencies = new int[256];
            frequencies[0] = 1;

            var tree = new HuffmanTree(frequencies);

            var ex = Assert.Throws<ArgumentNullException>(
                () => HuffmanCode.Decode(null, tree)
            );

            string messageExpected = "Encoded must not be null\r\nИмя параметра: encoded";
            string messageActual = ex.Message;

            Assert.AreEqual(messageExpected, messageActual);
        }

        [Test]
        public void DecodeMethod_ThrowsArgumentException_IfSourceIsEmpty()
        {
            // Prepare HuffmanTree
            int[] frequencies = new int[256];
            frequencies[0] = 1;

            var tree = new HuffmanTree(frequencies);

            var ex = Assert.Throws<ArgumentException>(
                () => HuffmanCode.Decode(new byte[] { }, tree)
            );

            string messageExpected = "Encoded must not be empty\r\nИмя параметра: encoded";
            string messageActual = ex.Message;

            Assert.AreEqual(messageExpected, messageActual);
        }

        [Test]
        public void EncodeMethod_WorksCorrectly()
        {
            byte[] source = new byte[] 
            {
                0,
                1, 1,
                2, 2, 2, 2,
                3, 3, 3, 3, 3,
                4, 4, 4, 4, 4, 4
            };

            string expected = "000000001111110110101010100101010101000000000000";
            string actual = HuffmanCode.Encode(source).ToBinaryString();

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void DecodeMethod_WorksCorrectly()
        {
            byte[] source = new byte[] { 0, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4 };

            byte[] encoded = new byte[] { 0, 253, 170, 149, 80, 0 };

            HuffmanCode.Encode(source);
            HuffmanTree tree = HuffmanCode.LastTree;

            byte[] decoded = HuffmanCode.Decode(encoded, tree);

            Assert.AreEqual(source, decoded);
        }
    }
}
