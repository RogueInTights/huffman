﻿using Huffman.Library.Utilities;
using NUnit.Framework;

namespace Huffman.Library.Tests.UnitTests
{
    [TestFixture]
    class ByteArrayExtensionsTests
    {
        [Test]
        public void ToByteArrayMethod_WorksCorrectly()
        {
            byte[] source = new byte[] { 2, 219, 251 };

            bool[] expected = new bool[]
            {
                false, false, false, false, false, false, true, false,
                true, true, false, true, true, false, true, true,
                true, true, true, true, true, false, true, true
            };
            bool[] actual = source.ToBoolArray();

            Assert.AreEqual(expected, actual);
        }
    }
}
