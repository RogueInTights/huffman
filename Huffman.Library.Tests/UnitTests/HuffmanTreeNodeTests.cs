﻿using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace Huffman.Library.Tests.UnitTests
{
    [TestFixture]
    class HuffmanTreeNodeTests
    {
        [Test]
        public void HuffmanTreeNode_Implements_IComparableInterface()
        {
            Type nodeType = typeof(HuffmanTreeNode);
            Type comparableType = typeof(IComparable);

            Assert.IsTrue(comparableType.IsAssignableFrom(nodeType));
        }

        [Test]
        public void CompareToMethod_WorksCorrectly()
        {
            var nodeA = new HuffmanTreeNode(0, 2);
            var nodeB = new HuffmanTreeNode(1, 1);

            Assert.IsTrue(nodeA.CompareTo(nodeB) > 0);

            nodeA = new HuffmanTreeNode(0, 1);
            nodeB = new HuffmanTreeNode(1, 2);

            Assert.IsTrue(nodeA.CompareTo(nodeB) < 0);

            nodeA = new HuffmanTreeNode(0, 1);
            nodeB = new HuffmanTreeNode(1, 1);

            Assert.IsTrue(nodeA.CompareTo(nodeB) == 0);
        }

        [Test]
        public void SortMethodInListOfNodes_SortsByAscendingFrequency()
        {
            List<HuffmanTreeNode> nodes = new List<HuffmanTreeNode>();

            nodes.Add(new HuffmanTreeNode(0, 2));
            nodes.Add(new HuffmanTreeNode(1, 3));
            nodes.Add(new HuffmanTreeNode(2, 1));

            nodes.Sort();

            Assert.AreEqual(1, nodes[0].Frequency);
            Assert.AreEqual(2, nodes[1].Frequency);
            Assert.AreEqual(3, nodes[2].Frequency);
        }

        [Test]
        public void HuffmanTreeNodeObject_CorrectlyConstructed_FromOtherTwoNodes()
        {
            var nodeA = new HuffmanTreeNode(0, 1);
            var nodeB = new HuffmanTreeNode(1, 2);

            var nodeAB = new HuffmanTreeNode(nodeA, nodeB);

            // Frequency
            int expectedFrequency = 3;
            int actualFrequency = nodeAB.Frequency;

            Assert.AreEqual(expectedFrequency, actualFrequency);

            // Symbol
            byte[] expectedSymbol = new byte[] { 0, 1 };
            byte[] actualSymbol = nodeAB.Symbol;

            Assert.AreEqual(expectedSymbol, actualSymbol);

            // Reference
            Assert.IsTrue(nodeA == nodeAB.Left);
            Assert.IsTrue(nodeB == nodeAB.Right);
        }
    }
}
