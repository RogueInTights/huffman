﻿using Huffman.Library.Utilities;
using NUnit.Framework;

namespace Huffman.Library.Tests.UnitTests
{
    [TestFixture]
    class BoolArrayExtensionsTests
    {
        [Test]
        public void ToBinaryStringMethod_WorksCorrectly()
        {
            bool[] boolArray = new bool[] { true, false, true };

            string expected = "101";
            string actual = boolArray.ToBinaryString();

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void ToByteArrayMethod_WorksCorrectly()
        {
            bool[] source = new bool[] 
            {
                false, false, false, false, false, false, true, false,
                true, true, false, true, true, false, true, true,
                true, true, true, true, true, false, true, true
            };

            byte[] actual = source.ToByteArray();
            byte[] expected = new byte[] { 2, 219, 251 };

            Assert.AreEqual(expected, actual);
        }
    }
}
