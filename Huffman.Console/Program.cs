﻿using Huffman.Library;
using Huffman.Library.Utilities;
using System.IO;
using Terminal = System.Console;

namespace Huffman.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            string path;

            Terminal.WriteLine("Specify path to file: ");
            path = Terminal.ReadLine();

            // TODO 1 : Compress and decompress file

            // тут колхоз (везде колхоз)
            byte[] source = File.ReadAllBytes(path);

            byte[] encoded = HuffmanCode.Encode(source).ToByteArray();
            File.WriteAllBytes(path + ".min", encoded);

            Terminal.Read();
        }
    }
}
