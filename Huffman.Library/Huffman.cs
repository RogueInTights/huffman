﻿using Huffman.Library.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Huffman.Library
{
    public static class HuffmanCode
    {
        public static HuffmanTree LastTree { get; private set; }

        public static bool[] Encode(byte[] source)
        {
            // валидация
            #region Validation

            if (source == null)
                throw new ArgumentNullException(nameof(source), "Source must not be null");

            if (source.Length == 0)
                throw new ArgumentException("Source must not be empty", nameof(source));

            #endregion

            // подсчет частот
            // Count frequencies
            int[] frequencies = new int[256];

            foreach (byte symbol in source)
                frequencies[symbol]++;

            LastTree = new HuffmanTree(frequencies);

            LastTree.CreateReplacementsTable();

            List<bool> result = new List<bool>();

            foreach (byte symbol in source)
                result.AddRange(LastTree.Replacements[symbol]);

            // выравнивание (добавление некоторого количества битов слева, чтоб кодированное сообщение было сравнимо по модулю с 8)
            // Padding
            int remain = result.Count % 8;

            bool[] padding = new bool[remain < 7 ? 8 - remain : 16 - remain];
            padding[padding.Length - 1] = true;

            result.InsertRange(0, padding);

            return result.ToArray();
        }

        public static byte[] Decode(byte[] encoded, HuffmanTree tree)
        {
            #region Validation

            if (encoded == null)
                throw new ArgumentNullException(nameof(encoded), "Encoded must not be null");

            if (encoded.Length == 0)
                throw new ArgumentException("Encoded must not be empty", nameof(encoded));

            #endregion

            // получаем значимую часть закодированного массива байтов
            // Get significant part of encoded byte array
            bool[] encodedBits = encoded.ToBoolArray();

            int significantPartIndex = 1;
            while (!encodedBits[significantPartIndex++]) continue;

            List<bool> significantPart = encodedBits.ToList();
            significantPart.RemoveRange(0, significantPartIndex);

            encodedBits = null;

            // декодируем с использованием созданного во время кодирования дерева
            // Decoding
            List<bool[]> replacements = tree.Replacements.ToList();

            List<byte> result = new List<byte>();

            List<bool> encodedSymbol = new List<bool>();

            foreach (var bit in significantPart)
            {
                encodedSymbol.Add(bit);

                int symbol = replacements.FindIndex(
                    bits => (bits == null) ? false : Enumerable.SequenceEqual(bits, encodedSymbol)   
                );

                if (symbol != -1)
                {
                    result.Add((byte)symbol);

                    encodedSymbol = new List<bool>();
                }
            }

            return result.ToArray();
        }
    }
}
