﻿using System;
using System.Collections.Generic;
using System.Linq;
using Node = Huffman.Library.HuffmanTreeNode;

namespace Huffman.Library
{
    public class HuffmanTree
    {
        private Node _root;

        public bool[][] Replacements { get; private set; }

        public HuffmanTree(int[] frequencies)
        {
            // валидация
            #region Validation

            string exceptionMessage = "An array of 256 elements is required";

            if (frequencies == null)
                throw new ArgumentNullException(nameof(frequencies), exceptionMessage);

            if (frequencies.Length != 256)
                throw new ArgumentException(exceptionMessage, nameof(frequencies));
            
            if (Enumerable.SequenceEqual(frequencies, new int[256]))
                throw new ArgumentException("Frequencies must contain at least one value greater than 0", nameof(frequencies));

            #endregion

            // TODO 0 : Use priority queue instead list
            List<Node> nodes = new List<Node>();

            // пилим узел для каждого байта
            for (byte i = 0; ; i++)
            {
                if (frequencies[i] != 0)
                    nodes.Add(new Node(i, frequencies[i]));

                if (i == 255) break;
            }

            // строим дерево из узлов
            while (nodes.Count > 1)
            {
                nodes.Sort();

                Node node = new Node(nodes[0], nodes[1]);

                nodes.RemoveRange(0, 2);
                nodes.Insert(0, node);
            }

            _root = nodes[0];
        }

        // метод для запила таблицы замен
        public void CreateReplacementsTable()
        {
            Replacements = Replacements ?? new bool[256][];

            CalculateReplacements(_root, new bool[] { });
        }

        // рекурсивный метод для запила таблицы замен
        private void CalculateReplacements(Node root, bool[] prefix)
        {
            int len = prefix.Length;

            // если у узла нет потомков
            if (root.Left == null)
            {
                byte symbol = root.Symbol[0];

                Replacements[symbol] = new bool[len];

                prefix.CopyTo(Replacements[symbol], 0);

                return;
            }

            bool[] newPrefix = new bool[len + 1];

            prefix.CopyTo(newPrefix, 0);

            // левое поддерево
            // Left subtree
            newPrefix[len] = true;

            CalculateReplacements(root.Left, newPrefix);

            // правое поддерево
            // Right subtree
            newPrefix[len] = false;

            CalculateReplacements(root.Right, newPrefix);
        }
    }
}