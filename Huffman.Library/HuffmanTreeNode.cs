﻿using System;

namespace Huffman.Library
{
    internal class HuffmanTreeNode : IComparable
    {
        public byte[] Symbol { get; private set; } // символ
        public int Frequency { get; private set; } // частота

        public HuffmanTreeNode Left { get; private set; } = null;
        public HuffmanTreeNode Right { get; private set; } = null;

        // конструктор из символа и частоты
        public HuffmanTreeNode(byte symbol, int frequency)
        {
            Symbol = new byte[] { symbol };
            Frequency = frequency;
        }

        // конструктор из двух узлов
        public HuffmanTreeNode(HuffmanTreeNode nodeA, HuffmanTreeNode nodeB)
        {
            Symbol = new byte[nodeA.Symbol.Length + nodeB.Symbol.Length];

            nodeA.Symbol.CopyTo(Symbol, 0);
            nodeB.Symbol.CopyTo(Symbol, nodeA.Symbol.Length);

            Frequency = nodeA.Frequency + nodeB.Frequency;

            Left = nodeA;
            Right = nodeB;
        }

        // реализация IComparable 
        public int CompareTo(object obj) => 
            Frequency - (obj as HuffmanTreeNode).Frequency;       
    }
}
