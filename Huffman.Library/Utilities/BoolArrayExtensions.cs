﻿namespace Huffman.Library.Utilities
{
    public static class BoolArrayExtensions
    {
        public static string ToBinaryString(this bool[] arr)
        {
            string result = "";

            foreach (bool bit in arr)
                result += bit ? '1' : '0';

            return result;
        }

        public static byte[] ToByteArray(this bool[] source)
        {
            int len = source.Length / 8;
            if (source.Length % 8 != 0) len++;

            byte[] result = new byte[len];

            int bitIndex = 7, byteIndex = 0;
            foreach (bool bit in source)
            {
                result[byteIndex] |= (byte)((bit ? 1 : 0) << bitIndex--);

                if (bitIndex == -1)
                {
                    bitIndex = 7;
                    byteIndex++;
                }
            }

            return result;
        }
    }
}
