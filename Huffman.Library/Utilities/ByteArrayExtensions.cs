﻿namespace Huffman.Library.Utilities
{
    public static class ByteArrayExtensions
    {
        public static bool[] ToBoolArray(this byte[] source)
        {
            bool[] result = new bool[source.Length * 8];

            int shift = 7, bitIndex = 0;
            for (int i = 0; i < source.Length; i++)
            {
                while (shift >= 0)
                {
                    result[bitIndex] = (source[i] & (1 << shift)) != 0;

                    bitIndex++;
                    shift--;
                }

                shift = 7;
            }

            return result;
        }
    }
}
